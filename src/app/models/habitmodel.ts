export interface HabitModel {
    name: string;
}

export interface DateModel extends firebase.firestore.DocumentData {
    date: firebase.firestore.Timestamp;
}