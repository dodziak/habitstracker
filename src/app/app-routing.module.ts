import { AddHabitComponent } from './components/main/main/add-habit/add-habit.component';
import { LoginComponent } from './components/login/login/login.component';
import { MainComponent } from './components/main/main/main.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HabitsComponent } from './components/main/main/habits/habits.component';


const routes: Routes = [
  {
    path: 'main', component: MainComponent,
    children: [
      {
        path: 'habits',
        component: HabitsComponent
      },
      {
        path: 'addHabit',
        component: AddHabitComponent
      }
    ]
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: '', redirectTo: '/login', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
