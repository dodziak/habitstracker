import { DateModel } from './../models/habitmodel';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { HabitModel } from '../models/habitmodel';
import { AuthService } from './auth.service';
import * as moment from 'moment';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class HabitsService {
  daysArr = [];

  habitsCollection: AngularFirestoreCollection<HabitModel>;
  habits: Observable<DocumentChangeAction<HabitModel>[]>;
  habitsDoc: AngularFirestoreDocument<HabitModel>;

  constructor(private db: AngularFirestore, private authService: AuthService) {
    this.authService.authState$.subscribe((user) => {
      if (user != null) {
        this.getHabitsList();
      }
    });

    for (let i = 0; i < 7; i++) {
      this.daysArr[i] = moment().subtract(i, 'days');
    }
  }

  get userId() {
    return this.authService.user.uid;
  }

  get pathToActivehabits() {
    return `habits/${this.userId}/activeHabits`;
  }

  getHabitsList() {
    this.habitsCollection = this.db.collection<HabitModel>(this.pathToActivehabits);
    this.habits = this.habitsCollection.snapshotChanges();
  }

  addHabit(name: string) {
    this.habitsCollection = this.db.collection<HabitModel>(this.pathToActivehabits);
    this.habitsCollection.add({ name });
    }

  deleteHabit(id: string) {
    this.habitsDoc = this.db.doc<HabitModel>(`${this.pathToActivehabits}/${id}`);
    this.habitsDoc.delete();
  }

  getDatesCollection(id: string) {
    return this.db.collection<DateModel>(`${this.pathToActivehabits}/${id}/dates`);
  }
  }


