import { HabitModel } from './../models/habitmodel';
import { User } from './../models/user';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase/app';
import { auth } from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoading = false;
  public user: firebase.User = null;

  get isUserLogged(): boolean {
    return !!(this.user && this.user.uid);
  }

  private readonly isLoadingSubject: Subject<boolean> = new Subject<boolean>();
  private readonly isLoadingSubscriber = this.isLoadingSubject.subscribe((isLoading) => this.isLoading = isLoading);
  readonly isLoading$: Observable<boolean> = this.isLoadingSubject.asObservable();

  readonly authState$: Observable<firebase.User | null> = this.fireAuth.authState;

  constructor(private fireAuth: AngularFireAuth, private db: AngularFirestore) {
    this.isLoadingSubject.next(true);
    this.fireAuth.auth.setPersistence('local');

    this.authState$.subscribe(user => {
      this.user = user;
      this.isLoadingSubject.next(false);
    });
  }

  oAuthProvider(provider) {
    return this.fireAuth.auth.signInWithPopup(provider)
        .catch((error) => {
            window.alert(error);
        });
  }

  signinWithGoogle() {
    const singInRequest = this.oAuthProvider(new auth.GoogleAuthProvider())
        .then(result => {
            console.log('Successfully logged in!');
        }).catch(error => {
            console.log(error);
        });

    return singInRequest;
  }

  signOut() {
    const logoutRequest = this.fireAuth.auth.signOut();
    return logoutRequest;
  }


}
