import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { Subscription, Subscriber } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private authSubscriver: Subscription = null;

  constructor(public auth: AuthService, private router: Router) {
    this.authSubscriver = this.auth.isLoading$.subscribe((value) => {
      if (!value && router.url.includes('login') && auth.isUserLogged) {
        router.navigateByUrl('/main/habits');
      }
    });

  }
}
