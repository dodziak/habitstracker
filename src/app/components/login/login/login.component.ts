import { Router } from '@angular/router';
import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public auth: AuthService, private router: Router, private db: AngularFirestore) { }

  ngOnInit() {
  }

  login() {
    this.auth.signinWithGoogle()
    .then((res) =>  {
      this.router.navigate(['main/habits']);
    })
    .catch(err => { console.error('Logowanie nieudane'); });


  }


}
