import { HabitModel } from 'src/app/models/habitmodel';
import { HabitsService } from 'src/app/services/habits.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-habit',
  templateUrl: './add-habit.component.html',
  styleUrls: ['./add-habit.component.scss']
})
export class AddHabitComponent implements OnInit {

  inputName: HabitModel['name'];

  constructor(private habSvc: HabitsService) { }

  ngOnInit() {
  }

  addHabit(name: string) {
    this.habSvc.addHabit(name);
    this.inputName = null;
    alert('Dodano pomyślnie !');
  }
}
