import { Router } from '@angular/router';
import { AuthService } from './../../../../services/auth.service';
import { HabitModel } from '../../../../models/habitmodel';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { HabitsService } from 'src/app/services/habits.service';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-habits',
  templateUrl: './habits.component.html',
  styleUrls: ['./habits.component.scss']
})
export class HabitsComponent implements OnInit {

  get habits() {
    return this.habSvc.habits;
  }

  get daysArr() {
    return this.habSvc.daysArr;
  }

  constructor(
    private habSvc: HabitsService,
    private db: AngularFirestore,
    public auth: AuthService,
    private router: Router) {



    }

  ngOnInit() {

  }

}
