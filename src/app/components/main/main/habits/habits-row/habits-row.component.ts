import { Component, OnInit, Input, ChangeDetectionStrategy, ViewRef } from '@angular/core';
import { HabitsService } from 'src/app/services/habits.service';
import { AngularFirestore, DocumentChangeAction, AngularFirestoreCollection, QuerySnapshot, DocumentData } from 'angularfire2/firestore';
import { AuthService } from 'src/app/services/auth.service';
import { HabitModel, DateModel } from 'src/app/models/habitmodel';
import { Observable } from 'rxjs';
import { ChangeDetectorRef } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-habits-row',
  templateUrl: './habits-row.component.html',
  styleUrls: ['./habits-row.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class HabitsRowComponent implements OnInit {

  isChecked: boolean = false;

  datesCollection: AngularFirestoreCollection<DateModel>;
  dates: QuerySnapshot<DocumentData>;
  dateObj: DateModel;

  get daysArr() {
    return this.habSvc.daysArr;
  }

  constructor(private habSvc: HabitsService,
              private auth: AuthService,
              private cdr: ChangeDetectorRef) { }

  @Input()
  habit: DocumentChangeAction<HabitModel>;

  ngOnInit() {
    const start = new Date();
    const end = new Date('2018-01-01');

    this.datesCollection = this.habSvc.getDatesCollection(this.habit.payload.doc.id);
    this.datesCollection.ref
      .orderBy('date', 'asc')
      .where('date', '>=', end)
      .where('date', '<=', start)
      .onSnapshot((res) => {
        this.dates = res;

        setTimeout(() => { // change detector after view destroyed
          if (this.cdr !== null && this.cdr !== undefined &&
            !(this.cdr as ViewRef).destroyed) {
            this.cdr.detectChanges();
          }
        }, 250);
      });

  }

  existForDate(date): boolean {
    for (let i = 0; i < this.dates.docs.length; i++) {
      if (this.dates.docs[i].data().date.toDate().setHours(0, 0, 0, 0) === date.toDate().setHours(0, 0, 0, 0)) {
        return true;
      }
    }
    return false;
  }



  markAsDone(date) {
    this.dateObj = {
      date: firebase.firestore.Timestamp.fromDate(date.toDate())
    };

    const dateExistsInDocs = this.dates.docs.find(
      (x => x.data().date.toDate().setHours(0, 0, 0, 0) === date.toDate().setHours(0, 0, 0, 0)));

    if (!dateExistsInDocs)  {
        this.datesCollection.add(this.dateObj);
    } else {
      this.datesCollection.doc(dateExistsInDocs.id).delete();
    }
}

  deleteHabit(id: string) {
    return this.habSvc.deleteHabit(id);
  }

}
