import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HabitsRowComponent } from './habits-row.component';

describe('HabitsRowComponent', () => {
  let component: HabitsRowComponent;
  let fixture: ComponentFixture<HabitsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitsRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HabitsRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
