import { Router } from '@angular/router';
import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private router: Router, private auth: AuthService) { }

  ngOnInit() {

  }


  navigateToAdd() {
    this.router.navigateByUrl('/main/addHabit');
  }

  navigateToHabits() {
    this.router.navigateByUrl('/main/habits');
  }

  logout() {
    this.auth.signOut();
    this.router.navigateByUrl('/login');
  }
}
